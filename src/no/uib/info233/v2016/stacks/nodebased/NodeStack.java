package no.uib.info233.v2016.stacks.nodebased;

import no.uib.info233.v2016.stacks.exceptions.EmptyStackException;
import no.uib.info233.v2016.stacks.interfaces.StackInterface;

public class NodeStack<T> implements StackInterface<T> {

private Node topNode;
	
	public NodeStack() {
		topNode = null;
	}
	
	@Override
	public void push(T newEntry) {
		Node newNode = new Node(newEntry, topNode);
		topNode = newNode;
	}

	@Override
	public T pop() {
		
		T top = peek();
		assert topNode != null;
		topNode = topNode.getNextNode();
		return top;
	}
	
	@Override
	public T peek() {
		if (!isEmpty())
			return topNode.getData();
		else	
			return null;
	}

	@Override
	public boolean isEmpty() {
		
		return topNode == null;
	}

	@Override
	public void clear() {
		topNode = null;
	}
	
	private class Node {
		private T data;
		private Node next;
		
		Node(T data, Node next) {
			this.data = data;
			this.next = next;
		}
		T getData () {
			return data;
		}
		Node getNextNode() {
			return next;
		}
	}

}
