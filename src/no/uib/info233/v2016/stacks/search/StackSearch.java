package no.uib.info233.v2016.stacks.search;

import java.util.ArrayList;

import no.uib.info233.v2016.stacks.nodebased.NodeStack;
import no.uib.info233.v2016.stacks.search.nodes.PathNode;

public class StackSearch {
	
	public static void main(String[] args) {
		StackSearch stack = new StackSearch();
		System.out.println(stack.isConnected("Moss", "Bergen"));
	}
	
	private PathNode n1;
	private PathNode n2;
	private PathNode n3;
	private NodeStack<PathNode> stack;
	private ArrayList<PathNode> nodes;
	
	public StackSearch() {
		n1 = new PathNode("Bergen");
		n2 = new PathNode("Oslo");
		n3 = new PathNode("Trondheim");
		stack = new NodeStack<>();
		nodes = new ArrayList<>();
		
		n1.addEdge(n2);
		n2.addEdge(n3);
		PathNode n4 = new PathNode("Bod�");
		PathNode n5 = new PathNode("Moss");
		
		n3.addEdge(n4);
		n2.addEdge(n5);
		
		nodes.add(n1);
		nodes.add(n2);
		nodes.add(n3);
		nodes.add(n4);
		nodes.add(n5);
	}
	
	public boolean isConnected(String label1, String label2) {
		boolean connected = false; 
		PathNode start = null;
		
		
		for (PathNode n : nodes) {
			if (n.getLabel().equals(label1)) {
				start = n;
				break;
			}
		}
		
		start.setVisited(true);
		
		stack.push(start);
		
		stackTrace(label2);
		
		if (checkNode(stack.peek(), label2)) connected = true;
		 	
		while (!stack.isEmpty()) {
			System.out.println(stack.pop().getLabel());
		}
		
		return connected;
		
	}

	private void stackTrace(String label2) {

		if (checkNode(stack.peek(), label2)) {
			return;
		} else {
			
			search(stack.peek(), label2);
		}
		
		
		
	}

	private boolean search(PathNode peek, String label2) {
		boolean connection = false;
		
		if (checkNode(peek, label2)) {
			connection = true;
			return connection;
		}
		
		int size = peek.getOutEdges().size();
		
		while (size > 0 && !connection) {
			PathNode n = peek.getNext();
			if (!n.isVisited()) {
				n.setVisited(true);
				stack.push(n);
				connection = search(n, label2);
				if (!connection) {
					stack.pop();
				}
			}
			size--;
		}
		
		return connection;

	}

	private boolean checkNode(PathNode n, String label2) {
		boolean target = false;
		if (n.checkLabel(label2)) target = true;
		return target;
	}

	
	
	
	

}
