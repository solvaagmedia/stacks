package no.uib.info233.v2016.stacks.search.nodes;

import java.util.ArrayList;

public class PathNode {
	
	private String label;
	private ArrayList<PathNode> outEdges;
	private boolean visited;
	private int get;
	
	
	public PathNode(String label) {
		this.label = label;
		this.outEdges = new ArrayList<>();
		this.visited = false;
		this.get = 0;
	}
	
	public PathNode(String label, PathNode next) {
		this.label = label;
		this.outEdges = new ArrayList<>();
		this.visited = false;
		this.get = 0;
		
		this.outEdges.add(next);
		
	}
	
	public boolean addEdge(PathNode edge){
		
		this.outEdges.add(edge);
		this.get = outEdges.indexOf(edge);

		if(!edge.hasEdge(this)) edge.addEdge(this);
		
		return outEdges.contains(edge);
		
	}
	
	public PathNode getNext(){
		PathNode result = null;
		
		result = outEdges.get(get);
		
		get--;
		
		return result;
	}
	
	public boolean hasEdge(PathNode edge){
		boolean exists = false;
		
		if (this.outEdges.contains(edge)) exists = true;
		
		return exists;
	}
	
	public boolean checkLabel(String test){
		return this.label.equals(test);
	}

	public boolean isVisited() {
		return visited;
	}

	public void setVisited(boolean visited) {
		this.visited = visited;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public ArrayList<PathNode> getOutEdges() {
		return outEdges;
	}

	public void setOutEdges(ArrayList<PathNode> outEdges) {
		this.outEdges = outEdges;
	}
	
	

}
