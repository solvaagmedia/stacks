package no.uib.info233.v2016.stacks.interfaces;

public interface StackInterface<T> {
	/** Adds a new entry to the top of this stack.
	 * @param newEntry  An object to be added to the stack.	 */
	public void push (T newEntry);
		
	/**
	 * Removes and returns this stack's top entry.
	 * @return The object at the top of the stack.
	 * @throws EmtyStackException if the stack is empty 
	 * 								before the operation. */
	public T pop();
	
	/**
	 * Retrieves this stack's top entry.
	 * @return The object at the top of the stack.
	 * @throws EmtpyStackException if the stack is empty.  */
	public T peek();
	
	/**
	 * Detects whether this stack is empty.
	 * @return True if the stack is emtpy. */
	public boolean isEmpty();
	
	/**
	 * Removes all entries from this stack. */
	public void clear();
}
